package com.fb;

import com.mongodb.BasicDBObject;
import com.mongodb.Block;
import com.mongodb.DBCursor;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import org.bson.Document;
import org.eclipse.jetty.util.ajax.JSON;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.UUID;
import java.util.regex.Pattern;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.*;


/**
 * Example Index HTML class using Javalin
 * <p>
 * Generate a static HTML page using Javalin by writing the raw HTML into a Java
 * String object

 */
public class Index implements Handler {
    public static String navbar(String search_value,String drop,String user,String logout_hidden)
    {
        int newdrop;
        String str="";
        // Add HTML for the logo.png image
        str=str+"<nav class=\"navbar navbar-expand-lg bg-dark navbar-dark text-white \">" +
                "  <ul class=\"navbar-nav\">" +
                "<img src=\"logo.png\" alt=\"Logo\" style=\"width:40px;\">" +

                "" ;
        str = str + "</ul>" +
                "</nav>";
        str=str+"<nav class=\"navbar navbar-expand-lg bg-dark navbar-dark text-white justify-content-center\">" +
                "  <ul class=\"navbar-nav\">" +
                "" +

                "" ;
        str = str + "<form action='/' method='post'>";
        str = str+"<li class='nav-item'><label for='drop'> Select type to search:</label>";
        str=str+"<select id='accomodates_drop' name='accomodates_drop'>";
        str=str+ " <option value=\"address\"> Location</option>";
        str=str+  " <option value=\"property_type\">Property Type</option>";
        str=str+ " <option value=\"price\">Price</option>";
        str=str+  " <option value=\"name\">Name</option>";
        str=str+  " <option value=\"amenities\">Amenities</option>";
        str=str+  " <option value=\"accommodates\">Accommodates</option>";
        str=str+  "<option value=\"bedrooms\">Bedrooms</option>";
        str=str+  "<option value=\"beds\">Beds</option>";
        str=str+  "<option value=\"review_scores_rating\">Review Score Rating</option>";
        str=str+  "<option value=\"host\">Host Status(Super host or regular host)</option>";
        str=str+  "<option value=\"summary\">Summary</option>";


        str=str+" </select>";
        str = str + "  ";
        str = str + "      <input  id='search_textbox' name='search_textbox' placeholder='search here' value='" + search_value + "'><button type='submit' class='btn btn-primary'>Search</button>";
        str = str + "   </li>";
        str = str + "</form>";

        if(user==null) {
            str=str+"";
            ;
            str = str + "<li class='nav-item '><button type=\"button\" class=\"btn btn-primary\" data-toggle=\"modal\" data-target=\"#myModalLogin\">\n" +
                    "  Login\n" +
                    "</button></li>";
        }
        str = str + "</ul>" +
                "</nav>";



        return str;
    }
    // URL of this page relative to http://localhost:7000/
    public static final String URL = "/";
    public static String usernames[] = {"ENTER", "ENTER","ABC"};
    public static String passwords[] = {"ENTER", "ENTER","PASS"};

    @Override
    public void handle(Context context) throws Exception {
        // Create a simple HTML webpage in a String
        String html = "<html>\n";
        String user = "";

        // Add some Header information
        html = html + "<head>" + "<title>Homepage</title>\n";

        // Add some CSS (external file)
        html = html + "  <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css\">\n" +
                "  <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js\"></script>\n" +
                "  <script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js\"></script>\n" +
                "  <script src=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js\"></script>  <link rel='stylesheet' type='text/css' href='common.css' />\n";

        // Add the body
        html = html + "<body>\n <div class=\"row\"><div class='col-sm-2 col-md-2 col-lg-2'></div>";
        html=html+"<div class='col-sm-8 col-md-8 col-lg-8' style=\"padding: 10px;box-shadow: .10px .10px 20px grey; margin-bottom: 30px;border: 0px Solid black;\">";
        String search_value = context.formParam("search_textbox");
        String drop = context.formParam("accomodates_drop");
        user = Util.getLoggedInUser(context);
        String logout_hidden = context.formParam("logout_hidden");
        String nav=navbar(search_value,drop,user,logout_hidden);
        html = html +nav+"</div></div><div class='container'>";
        int count=MongodbConnection.countrow();
        int pagesize=count/10;
        String start=context.queryParam("start");
        String end=context.queryParam("end");
        int startloop=1;int endloop=10;
        if(end==null)
        {

        }
        else
        {
            endloop=Integer.parseInt(end);

        }
        if(start==null)
        {

        }
        else
        {
            startloop=Integer.parseInt(start);

        }
        String pagestr=context.queryParam("page");
        System.out.println("context.queryParam(\"page\")  "+context.queryString()+"          "+context.queryParam("page"));
        int page=0;
        if(pagestr==null)
        {

        }
        else
        {
            page=Integer.parseInt(pagestr);search_value=context.queryParam("search_value");drop=context.queryParam("drop");



        }
        ArrayList<String> apartments = new ArrayList<String>();
        html = html + "<h1>Apartments</h1>\n";
        int newdrop;
        if (search_value != null) {
            if (drop.equals("accommodates") || drop.equals("price") || drop.equals("bedrooms") || drop.equals("beds") || drop.equals("review_scores_rating")) {

                newdrop = Integer.parseInt(search_value);


                MongoCursor<Document> cursor = MongodbConnection.paging1(page,newdrop,drop);
                while(cursor.hasNext())
                {

                    Document record = cursor.next();
                    Document img= (Document) record.get("images");
                    String bedroom="";
//                   if(record.get("bedrooms").toString()!=null)
//                   {
//                               bedroom=record.get("bedrooms").toString();
//
//
//                   }
                    html=html+"<div> \n";
                    html=html+ "<table>\n";
                    html=html+ "<td>"+"<img width='800px' height='350px' src='" +img.get("picture_url").toString()+"'/>"+"</td>";

                    html=html+ "<td>"+"<h3>"+record.get("name").toString()+"</h3>"+"</h5>"+"</br>";
                     html=html+"<h5>"+"Price: $"+record.get("price")+"</br>";
                     html=html +"<h5>"+"Bedrooms: "+record.get("bedrooms")+"</h5>"+"</br>";
                    html=html+"<h5>"+"Number of beds: "+record.get("beds")+"</h5>";
                    html = new StringBuilder().append(html).append("<li><a href='/page6.html?id=").append(record.get("_id").toString()).append("'>").append("View Details...").append("</a> </li>").toString() + "";
                    html=html+"</td>";
                    html=html+ "</tr>\n";

                    html=html+ "</table>"+"</br>\n";
                    html=html+"</div> \n";


                }




//

            } else {


                MongoCursor<Document> iterator =MongodbConnection.paging(page,search_value,drop);

                while (iterator.hasNext()) {

                    Document record = iterator.next();
                    Document img= (Document) record.get("images");
                    String bedroom="";
//                    if(record.get("bedrooms").toString())
//                    {
//                        bedroom=record.get("bedrooms").toString();
//
//
//                    }
                    html=html+"<div> \n";
                    html=html+ "<table>\n";
                    html=html+ "<td>"+"<img width='800px' height='350px' src='" +img.get("picture_url").toString()+"'/>"+"</td>";

                    html=html + "<td>"+"<h3>"+record.get("name").toString()+"</h3>"+"</h5>"+"</br>";
                     html=html+"<h5>"+"Price: $"+record.get("price")+"</br>";
                    html=html +"<h5>"+"Bedrooms: "+bedroom+"</h5>"+"</br>";
                     html=html+"<h5>"+"Number of beds: "+record.get("beds")+"</h5>";
                    html = new StringBuilder().append(html).append("<li><a href='/page6.html?id=").append(record.get("_id").toString()).append("'>").append("View Details...").append("</a> </li>").toString() + "";
                    html=html+"</td>";
                    html=html+ "</tr>\n";

                    html=html+ "</table>"+"</br>\n";
                    html=html+"</div> \n";

                }




            }
            // Finish the List HTML
            html = html + "</ul>\n";
            html=html+"<ul class=\"pagination\">";
            for (int i = startloop; i <= endloop; i++) {
                html = new StringBuilder().append(html).append("<li><a href='/?page=").append(i).append("&drop=").append(drop).append("&search_value=").append(search_value).append("'>").append("   Page ").append(i).append(" ").append("   </a></li>    ").toString() + "";

            }

            html=html+"</ul><ul class=\"pager\">\n" ;
            html = new StringBuilder().append(html).append("<li><a href='/?page=").append(endloop).append("&drop=").append(drop).append("&start=").append(startloop+10).append("&end=").append(endloop+10).append("&search_value=").append(search_value).append("'>").append("Next-> ").append("</a></li>    ").toString() + "";
            if(endloop>11)    {html = new StringBuilder().append(html).append("<li><a href='/?page=").append(endloop).append("&drop=").append(drop).append("&start=").append(startloop-10).append("&end=").append(endloop-10).append("&search_value=").append(search_value).append("'>").append("<-Previous ").append("</a></li>    ").toString() + "";}
            html=html+ "</ul>";
        }
///


        //display login form if user is not logged in





        if(user == null) {

            // get username and password (and hidden field to identify login button pressed)
            String username_textbox = context.formParam("username_textbox");
            String password_textbox = context.formParam("password_textbox");
            String login_hidden = context.formParam("login_hidden");

            if(username_textbox == null){
                username_textbox = "";
            }
            html = html + " <div class=\"modal\" id=\"myModalLogin\">\n" +
                    "  <div class=\"modal-dialog\">\n" +
                    "    <div class=\"modal-content\">\n" +
                    "\n" +
                    "      <!-- Modal Header -->\n" +
                    "      <div class=\"modal-header\">\n" +
                    "        <h4 class=\"modal-title\">Login</h4>\n" +
                    "        <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n" +
                    "      </div>\n" +
                    "\n" +
                    "      <!-- Modal body -->\n" +
                    "      <div class=\"modal-body\"> <div class=\"container p-3 my-3 border \" style=\"padding: 10px;box-shadow: .10px .10px 20px grey; margin-bottom: 30px;border: 0px Solid black;\"><form action='/' method='post'>\n";

            html = html + "  <div class='form-group'>\n";
            html = html + "      <label for='username_textbox'>Username</label>\n";
            html = html + "      <input class='form-control  id='username_textbox' name='username_textbox' placeholder='username' value='" + username_textbox + "'>\n";
            html = html + "   </div>\n";
            html = html + "   <div class='form-group'>\n";
            html = html + "      <label for='password_textbox'>Password</label>\n";
            html = html + "      <input class='form-control  id='password_textbox' name='password_textbox' placeholder='password'>\n";
            html = html + "   </div>\n";
            html = html + "   <input type='hidden' id='login_hidden' name='login_hidden' value='true'>";
            html = html + "   <button type='submit' class='btn btn-primary'>Login</button>\n";
            html = html + "</form></div></div>\n" +
                    "\n" +
                    "      <!-- Modal footer -->\n" +
                    "      <div class=\"modal-footer\">\n" +
                    "        <button type=\"button\" class=\"btn btn-danger\" data-dismiss=\"modal\">Close</button>\n" +
                    "      </div>\n" +
                    "\n" +
                    "    </div>\n" +
                    "  </div>\n" +
                    "</div>\n";

            // if login button pressed proceess username and password fields
            if(login_hidden != null && login_hidden.equals("true")){
                if (username_textbox == null || username_textbox == "" || password_textbox == null || password_textbox == "") {
                    // If username or password NULL/empty, prompt for authorisation details
                    html = html + "Enter a username and password\n";
                } else {
                    // If NOT NULL, then test password for match against array of users (in your code you will run a query)!
                    if(checkPassword(context, username_textbox, password_textbox)) {
                        //matching password found, reload page
                        context.redirect("/");
                    } else {
                        // no password match found
                        html = html + "invalid username/password";
                    }
                }
            }
        }
        else {
            // user is logged in - check if logout button pressed


            if (logout_hidden != null && logout_hidden.equals("true")) {
                // logout clicked
                logout(context);
            } else {
                // logout not clicked - show logout button
                html = html + "           <div></div>USER: " + "<div>" + user + "</div>";
                html = html + "<form action='/' method='post'>\n";
                html = html + "   <input type='hidden' id='logout_hidden' name='logout_hidden' value='true'>";
                html = html + "   <button type='submit' class='btn btn-primary'>Logout</button>\n";
                html = html + "</form>\n";








                // show logged in page content
                // Add HTML for the list of pages
                html = html + "<h1>Homepage</h1>" + "<p>Links to sub-pages</p>" + "<ul>\n";

                // Link for each page
                html = html + "<li> <a href='page1.html'>Page 1</a> </li>\n";
                html = html + "<li> <a href='page2.html'>Page 2</a> </li>\n";
                html = html + "<li> <a href='page3.html'>Page 3</a> </li>\n";
                html = html + "<li> <a href='page4.html'>Page 4</a> </li>\n";
                html = html + "<li> <a href='page5.html'>Page 5</a> </li>\n";
                html = html + "<li> <a href='page6.html'>Page 6</a> </li>\n";

                // Finish the List HTML
                html = html + "</ul>\n";

            }
        }

        // Finish the HTML webpage
        html = html + "</div></body>" + "</html>\n";

        // DO NOT MODIFY THIS
        // Makes Javalin render the webpage
        context.html(html);
    }

    // check if username and password matches agains array of users (in your code, this needs to query the database)
    public boolean checkPassword(Context context, String username, String password){
        boolean passwordMatchFound = false;
        System.out.println("this-->"+username+"     "+password);
        for(int i=0; i<usernames.length; i++){
            if(usernames[i].equalsIgnoreCase(username) && passwords[i].equals(password)){
                // match found - login the user
                login(context, username);
                passwordMatchFound = true;
            }
        }
        return passwordMatchFound;
    }

    // logout the current user by removing their cookie and session id details
    public void logout(Context context) {
        String id = context.cookie("id");
        context.sessionAttribute(id);
        context.removeCookie("id");
        context.sessionAttribute(id, null);
        // reload the page
        context.redirect("/");
    }

    // login the user by creating a random user id and associating in their cookie and session
    public void login(Context context, String username){
        String id = UUID.randomUUID().toString();
//      context.sessionAttribute("id", id);
        context.sessionAttribute(id, username);
        context.cookie("id", id);
    }
}
