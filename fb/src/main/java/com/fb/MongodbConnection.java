package com.fb;

import com.mongodb.BasicDBObject;
import com.mongodb.client.*;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Date;

import static com.mongodb.client.model.Filters.and;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.*;

public class MongodbConnection {
  static  MongoClient mongoClient = MongoClients.create("mongodb://localhost:27017");
  static  MongoDatabase mongoDatabase = mongoClient.getDatabase("AirBnB");
    public static  MongoCollection<Document> getCollection() {

        MongoCollection<Document> collection = mongoDatabase.getCollection("listingsAndReviews");
        return collection;
    }
    public static ArrayList<String> getAllApartmentNames(){
        MongoCollection<Document> collection = MongodbConnection.getCollection();
        MongoCursor<Document> cursor = collection.find(eq("property_type", "Apartment")).projection(fields(include())).iterator();
        ArrayList<String> apartments = new ArrayList<>();
        System.out.println("testing");
        try {

            while (cursor.hasNext()) {

                apartments.add(cursor.next().get("name").toString());
            //    System.out.println("hello------>>>>>>>-"+cursor.next().toString());
            }
        }
        finally {
            cursor.close();
        }
        return apartments;
    }
    public static  MongoCursor<Document>  getapartment(String id){
        MongoCollection<Document> collection = MongodbConnection.getCollection();
        MongoCursor<Document> cursor = collection.find(eq("_id", id)).projection(fields(include())).iterator();

//   ArrayList<String> apartments = new ArrayList<>();
        System.out.println("testing");

        return cursor;
    }
    public static  void addreview(String id,String review,String user){
        MongoCollection<Document> collection1 = MongodbConnection.getCollection();
    //    int a =(int) Math.random()*(9999999-10000000+1)+10000000;

        Document reviews = new Document().append("date", new Date())
                .append("listing_id",id).append("reviewer_id",user).append("reviewer_name",user).append("comments",review);

        collection1.updateOne(eq("_id", id),Updates.addToSet("reviews", reviews));
        // Insert the document

//   ArrayList<String> apartments = new ArrayList<>();
        System.out.println("testing");


    }
    public static  MongoCursor<Document>  paging(int page,String search_value,String drop){
        MongoCollection<Document> mongodb = MongodbConnection.getCollection();

        BasicDBObject regexQuery = new BasicDBObject();
        regexQuery.put(drop, new BasicDBObject("$regex", search_value+".*").append("$options", "i"));
        System.out.println("Drop----------------------------->"+drop+"      ");
        FindIterable<Document> cursor = mongodb.find(regexQuery).skip(10*(page-1)).limit(10);
        MongoCursor<Document> iterator = cursor.iterator();

        return iterator;
    }
    public static  MongoCursor<Document>  paging1(int page,int search_value,String drop){
        MongoCollection<Document> mongodb = MongodbConnection.getCollection();

        MongoCursor<Document> cursor = mongodb.find(eq(drop, search_value)).projection(fields(include())).iterator();


        return cursor;
    }
    public static  MongoCursor<Document>  viewreview(){
        MongoCollection<Document> collection1 = mongoDatabase.getCollection("reviews");
        MongoCursor<Document> cursor = collection1.find().projection(fields(include())).iterator();

//   ArrayList<String> apartments = new ArrayList<>();
        System.out.println("testing");

        return cursor;
    }
    public static int countrow(){
        MongoCollection<Document> collection = MongodbConnection.getCollection();
        MongoCursor<Document> cursor = collection.find().projection(fields()).iterator();
        int count=0;
        System.out.println("testing");
        try {

            while (cursor.hasNext()) {


                cursor.next().toString();
                count++;
            }
        }
        finally {
            cursor.close();
        }
        return count;
    }
//    public static  void addreview(String id,String review,String user){
//        MongoCollection<Document> collection1 = mongoDatabase.getCollection("reviews");
//        Document document
//                = new Document("user",
//                user)
//                .append("review",
//                        review).append("aid",
//                        id);
//
//        // Insert the document
//        collection1.insertOne(document);
////   ArrayList<String> apartments = new ArrayList<>();
//        System.out.println("testing");
//
//
//    }
    public static  void editreview(String id,String review){
        MongoCollection<Document> collection1 = mongoDatabase.getCollection("reviews");
        collection1.updateOne(
                Filters.eq("_id", id),
                Updates.set("review", review));
        System.out.println(
                "Successfully updated"
                        + " the document");
//   ArrayList<String> apartments = new ArrayList<>();
        System.out.println("testing");


    }
    public static ArrayList<String> getApartmentNamesByAccomodates(int accomodates){
        MongoCollection<Document> collection = MongodbConnection.getCollection();
        MongoCursor<Document> cursor = collection.find(and(eq("accommodates", accomodates), eq("property_type", "Apartment"))).projection(fields(include())).iterator();
        ArrayList<String> apartments = new ArrayList<String>();
        try {
            while (cursor.hasNext()) {
                Document record = cursor.next();
                String name = record.get("name").toString();
                String address = record.get("address").toString();
                apartments.add(name);
//            apartments.add(cursor.next().toJson());
            }
        }
        finally {
            cursor.close();
        }
        return apartments;
    }

}
