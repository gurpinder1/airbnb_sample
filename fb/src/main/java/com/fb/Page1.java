package com.fb;

import java.util.ArrayList;

import io.javalin.http.Context;
import io.javalin.http.Handler;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import com.mongodb.client.MongoCursor;
import org.springframework.web.bind.annotation.RequestParam;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.*;
/**
 * Temporary HTML as an example page.
 *
 * Based on the Project Workshop code examples.
 * This page currently:
 *  - Provides a link back to the index page
 *
 *
 */
public class Page1 implements Handler {

    // URL of this page relative to http://localhost:7000/
    public static final String URL = "/page1.html";

    @Override
    public void handle(Context context) throws Exception {
        // Create a simple HTML webpage in a String
        String user = Util.getLoggedInUser(context);
        //display login form if user is not logged in
        if(user == null) {
            context.redirect("/");
        }
        else {
            boolean flag = true;
            if (flag == true) {
                String html = "<html>\n";

                // Add some Header information
                html = html + "<head>" + "<title>Page 1: All Apartments</title>\n";

                // Add some CSS (external file)
                html = html + "<link rel = \"stylesheet\" href = \"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\"/>  <link rel='stylesheet' type='text/css' href='common.css' />\n";

                // Add the body
                html = html + "<body>\n";

                html = html + "USER: " + "<div class='container'>" + user + "";
                // Add HTML for link back to the homepage
                html = html + "<h1>Page 1: All apartments</h1>\n";
                html = html + "<p>Return to Homepage: \n";
                html = html + "<a href='/'>Link to Homepage</a>\n";
                html = html + "</p>\n";
//
                ArrayList<String> apartments1 = MongodbConnection.getAllApartmentNames();
                //     ArrayList<String> apartments = mongodb.getAllApartmentNames();
                // Add HTML for the movies list
                html = html + "<h1>Apartments</h1>" + "<ul>\n";

                // Finally we can print out all of the movies
                for (var apartment : apartments1) {
                    System.out.println(apartment);
                    html = new StringBuilder().append(html).append("<li><a href='/page6.html?apartment=").append(apartment).append("'>").append(apartment).append("</a> </li>").toString() + "";
                }

                // Finish the List HTML
                html = html + "</ul>\n";

                // Finish the HTML webpage
                html = html + "</div></body>" + "</html>\n";

                // DO NOT MODIFY THIS
                // Makes Javalin render the webpage

                context.html(html);
            }
            else {

            }
        }
    }

}


