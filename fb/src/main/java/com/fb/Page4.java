package com.fb;


import io.javalin.http.Context;
import io.javalin.http.Handler;

/**
 * Temporary HTML as an example page.
 *
 * Based on the Project Workshop code examples.
 * This page currently:
 *  - Provides a link back to the index page
 *
 */
public class Page4 implements Handler {

    // URL of this page relative to http://localhost:7000/
    public static final String URL = "/page4.html";

    @Override
    public void handle(Context context) throws Exception {
        // Create a simple HTML webpage in a String
        String user = Util.getLoggedInUser(context);
        //display login form if user is not logged in
        if(user == null) {
            context.redirect("/");
        }
        else {
            String html = "<html>\n";

            // Add some Header information
            html = html + "<head>" + "<title>Page 4: All Apartments</title>\n";

            // Add some CSS (external file)
            html = html + "<link rel='stylesheet' type='text/css' href='common.css' />\n";

            // Add the body
            html = html + "<body>\n";
            html = html + "USER: " + "<div>" + user + "</div>";

            // Add HTML for link back to the homepage
            html = html + "<h1>Page 4: All apartments</h1>\n";
            html = html + "<p>Return to Homepage: \n";


            // Finish the HTML webpage
            html = html + "</body>" + "</html>\n";

            // DO NOT MODIFY THIS
            // Makes Javalin render the webpage

            context.html(html);
        }
    }
}

