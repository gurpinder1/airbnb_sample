package com.fb;


import com.mongodb.client.MongoCursor;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import org.bson.Document;

import java.util.ArrayList;

/**
 * Temporary HTML as an example page.
 *
 * Based on the Project Workshop code examples.
 * This page currently:
 *  - Provides a link back to the index page
 *
 */
public class Page6 implements Handler {

    // URL of this page relative to http://localhost:7000/
    public static final String URL = "/page6.html";

    @Override
    public void handle(Context context) throws Exception {
        // Create a simple HTML webpage in a String
        String user = Util.getLoggedInUser(context);
        //display login form if user is not logged in

        String html = "<html>\n";

        // Add some Header information
        html = html + "<head>" + "<title>Page 6: All Apartments</title>\n";

        // Add some CSS (external file)
        html = html + "<link rel = \"stylesheet\" href = \"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css\"/>  <link rel='stylesheet' type='text/css' href='common.css' />\n";
        // String apartmentname="";
        //;
        System.out.println(" -----------------------------------------------"+context.formParam("apartment")+"   ");
        String id_main = context.queryParam("id");
        html=html+"</head>";
        // Add the body
        html = html + "<body>\n";
        if(user!=null)
        {
            html = html + "USER: " + "<div>" + user + "</div>";
        }


        // Add HTML for link back to the homepage
        html = html + "<h1>"+id_main+"</h1>\n";
        html = html + "<p>Return to Homepage: \n";
        html = html + "<a href='/'>Link to Homepage</a>\n";
        html = html + "</p>\n";
String hoststatus="";

        try (MongoCursor<Document> cursor = MongodbConnection.getapartment(id_main)) {
            if (cursor.hasNext()) {
                Document record = cursor.next();
                Document img= (Document) record.get("images");
                Document rev= (Document) record.get("review_scores");
                Document add= (Document) record.get("address");
                Document host= (Document) record.get("host");
                Object Review_view= record.get("reviews");


                System.out.println("REVIEW------------------------------>"+Review_view);

                Boolean host_status=(Boolean)host.get("host_is_superhost");
//
                if(host_status)
                {
                 hoststatus="Super Host" ;
                }
                else
                {
                    hoststatus="Regular Host" ;
                }
                        html=html+"<div class='row'><div class=' col-sm-1 col-md-1 col-lg-1'></div><div class='container-fluid col-sm-10 col-md-10 col-lg-10' style=\"padding: 10px;box-shadow: .10px .10px 20px grey; margin-bottom: 30px\"> \n";
                html=html+"<h2>"+""+record.get("property_type").toString()+"</h2>";
                html=html+ "<table style=\"border: 15px white;border-spacing: 30px;\">\n";
                html=html+ "<td>"+"<img width='800px' height='500px' src='" +img.get("picture_url").toString()+"'/>"+"</td>";

                html=html + "<td>"+"<h1>"+record.get("name").toString()+"</h1>"+""+"</br>";

                 html=html +"<h5>"+" "+record.get("bedrooms")+" Bedrooms ,"+" "+record.get("beds").toString()+" beds</h5>";

                html=html+"<h2>"+"Price: $"+record.get("price")+"</h2></br>";
                html=html+"<h5>"+"Review Score Rating: "+record.get("review_scores_rating")+"</h5>";


                html=html+"<h5>"+"Accommodates: "+record.get("accommodates")+"</h5>";
                html=html+"<h5>"+"Host status: "+hoststatus+"</h5>";
                html=html+"<h4>"+""+add.get("street")+" "+add.get("suburb").toString()+" "+add.get("government_area")+" "+add.get("market")+" "+add.get("country")+""+"</h4>";

                html=html+"</td>";
                html=html+ "</tr>\n";

                html=html+ "</table>\n";
                html=html+"<h3>"+"Amenities: "+record.get("amenities").toString()+"</h3>";
                html=html+"<h3>"+"Summary: "+record.get("summary").toString()+"</h3>";
                html=html+"</div> </div>\n";

            }
        }
        finally {
            //cursor.close();
        }




        context.html(html);

    }
}

